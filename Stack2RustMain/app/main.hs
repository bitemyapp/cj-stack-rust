module Main where

import           Prelude

import           MyDependency

main :: IO ()
main = do
  n <- read <$> getLine
  let n' = myDependencyFn n
  print n'
