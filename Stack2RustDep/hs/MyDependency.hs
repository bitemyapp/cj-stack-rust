{-# LANGUAGE ForeignFunctionInterface #-}
module MyDependency where

import           Prelude

import           Foreign.C
import           Foreign.Ptr
import           Foreign.ForeignPtr

import           Data.Word
import           Data.Coerce
import           System.IO.Unsafe

foreign import ccall "my_dependency_fn" c_my_dependency_fn :: CUChar -> IO CUChar

myDependencyFn :: Word8 -> Word8
myDependencyFn = coerce . unsafePerformIO . c_my_dependency_fn . coerce
