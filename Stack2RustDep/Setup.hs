import           Distribution.Simple
import           Distribution.Simple.Setup
import           Distribution.Types.LocalBuildInfo
import           Distribution.Types.GenericPackageDescription
import           Distribution.PackageDescription
import           Distribution.Types.HookedBuildInfo
import           Distribution.Types.BuildInfo
import           System.Process
import           System.Directory
import           Distribution.Types.Library

addExtraLibDir :: FilePath -> Library -> Library
addExtraLibDir dir lib = lib
  { libBuildInfo = (libBuildInfo lib)
                     { extraLibDirs = (dir ++ "/target/release")
                                        : (extraLibDirs . libBuildInfo $ lib)
                     }
  }

make
  :: (  (GenericPackageDescription, HookedBuildInfo)
     -> ConfigFlags
     -> IO LocalBuildInfo
     )
  -> (GenericPackageDescription, HookedBuildInfo)
  -> ConfigFlags
  -> IO LocalBuildInfo
make configHook pkgDesc flags = do
  _     <- readProcess "cargo" ["build", "--lib", "--release"] ""
  binfo <- configHook pkgDesc flags
  dir   <- getCurrentDirectory
  pure $ binfo
    { localPkgDescr = (localPkgDescr binfo)
                        { library = addExtraLibDir dir
                                      <$> (library . localPkgDescr $ binfo)
                        }
    }

main = defaultMainWithHooks simpleUserHooks
  { confHook = (make $ confHook simpleUserHooks)
  }
