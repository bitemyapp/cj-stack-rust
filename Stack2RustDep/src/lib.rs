#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}

#[no_mangle]
pub extern "C" fn my_dependency_fn(stuff: u8) -> u8 {
    stuff + 1
}
